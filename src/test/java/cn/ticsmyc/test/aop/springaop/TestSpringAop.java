package cn.ticsmyc.test.aop.springaop;

import cn.ticsmyc.test.aop.Echo;
import cn.ticsmyc.test.aop.EchoInterface;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;
import org.springframework.aop.target.SingletonTargetSource;

/**
 * @author Ticsmyc
 * @date 2020-12-21 22:20
 */
public class TestSpringAop {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Echo echo = new Echo();

        ProxyFactory proxyFactory = new ProxyFactory();

        //强制使用cglib
        proxyFactory.setProxyTargetClass(true);
        proxyFactory.setTargetSource(new SingletonTargetSource(echo));
//        //指定jdk动态代理的接口
//        proxyFactory.addInterface(EchoInterface.class);

        MyBeforeAdvice myBeforeAdvice = new MyBeforeAdvice();
        MyAroundAdvice myAroundAdvice = new MyAroundAdvice();

        NameMatchMethodPointcut nameMatchMethodPointcut = new NameMatchMethodPointcut();
        nameMatchMethodPointcut.addMethodName("test");
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor(nameMatchMethodPointcut, myBeforeAdvice);

        proxyFactory.addAdvisor(defaultPointcutAdvisor);
        proxyFactory.addAdvice(myAroundAdvice);

        EchoInterface proxy = (EchoInterface)proxyFactory.getProxy();
        proxy.test();

    }

}
