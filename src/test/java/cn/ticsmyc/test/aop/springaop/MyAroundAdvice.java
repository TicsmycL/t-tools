package cn.ticsmyc.test.aop.springaop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * @author Ticsmyc
 * @date 2020-12-22 16:37
 */
public class MyAroundAdvice implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Method method = invocation.getMethod();
        System.out.println(method.getName()+"执行前");

        try{
            Object proceed = invocation.proceed();
            System.out.println("访法执行结束" + proceed);
            return proceed;
        }catch (Exception e){
            System.out.println("异常" + e);
            return null;
        }finally{
            System.out.println("方法返回后");
        }
    }
}
