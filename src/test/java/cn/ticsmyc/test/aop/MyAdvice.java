package cn.ticsmyc.test.aop;

import cn.ticsmyc.tools.aop.EnhanceInfo;
import cn.ticsmyc.tools.aop.EnhancementAdvice;

/**
 * @author Ticsmyc
 * @date 2020-12-21 22:19
 */
public class MyAdvice implements EnhancementAdvice {
    @Override
    public boolean preInvoke(EnhanceInfo enhanceInfo) {
        System.out.println("执行前");
        return false;
    }

    @Override
    public void postInvoke(EnhanceInfo enhanceInfo) {
        System.out.println("执行后，可以获取到返回值:" + enhanceInfo.getResult());
        enhanceInfo.setResult("返回值被修改了");
    }

    @Override
    public void postReturning(EnhanceInfo enhanceInfo) {
        System.out.println("方法返回后， 在这里释放资源");
    }

    @Override
    public void postThrowing(EnhanceInfo enhanceInfo) {
        System.out.println("抛出了异常"+enhanceInfo.getException());
    }
}
