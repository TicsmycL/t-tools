package cn.ticsmyc.test.aop;

import cn.ticsmyc.tools.aop.EnhanceProxyCreater;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Ticsmyc
 * @date 2020-12-21 22:20
 */
public class TestAop {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Echo echo = new Echo();
        MyAdvice myAdvisor = new MyAdvice();
        Object proxy = EnhanceProxyCreater.getProxy(echo, myAdvisor);

        Method test = EchoInterface.class.getDeclaredMethod("test", null);
        test.invoke(proxy,null);

    }

}
