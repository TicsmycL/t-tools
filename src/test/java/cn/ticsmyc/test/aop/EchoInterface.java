package cn.ticsmyc.test.aop;

/**
 * @author Ticsmyc
 * @date 2020-12-21 22:46
 */
public interface EchoInterface {
    void test();
}
