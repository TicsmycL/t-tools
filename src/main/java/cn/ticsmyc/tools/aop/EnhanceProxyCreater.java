package cn.ticsmyc.tools.aop;

import net.sf.cglib.proxy.Enhancer;

import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;

/**
 * @author Ticsmyc
 * @date 2020-12-21 21:52
 */
public class EnhanceProxyCreater {

    /**
     * 获取代理类或者原始类型的实例（由注解决定）
     * @param target 被代理的方法
     * @param advisor 需要进行的增强
     * @return
     */
    public static Object getProxy(Object target, EnhancementAdvice advisor){
        TicAopHandler aopHandler = new TicAopHandler(target, advisor);

        Object proxyResult=null;
        if(Modifier.isFinal(target.getClass().getModifiers())){
            //如果是final类， 只能使用jdk代理
            if(target.getClass().getInterfaces().length !=0){
                proxyResult = getJdkProxy(target,aopHandler);
            }else{
                throw new RuntimeException("无法代理");
            }

        }else{
            //其他情况使用cglib
            proxyResult = getCglibProxy(target,aopHandler);
        }
        return proxyResult;
    }

    private static Object getCglibProxy(Object target, TicAopHandler aopHandler) {
        Enhancer enhancer = new Enhancer();
        //设置代理类的父类（就是被代理的类）
        enhancer.setSuperclass(target.getClass());
        //设置回调（代理类调用的真实方法（内含aop逻辑））
        enhancer.setCallback(aopHandler);
        return enhancer.create();
    }

    private static Object getJdkProxy(Object target, TicAopHandler aopHandler) {
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),aopHandler);
    }
}
