package cn.ticsmyc.tools.aop;

/**
 * 在方法执行前，抛出异常时，方法执行完成后，返回结果后  四个joinPoint上进行增强
 * @author Ticsmyc
 * @date 2020-12-21 21:42
 */
public interface EnhancementAdvice {


    /**
     * 在方法执行前调用
     * 如果经过一系列检查，发现不需要调用该方法，可以直接将结果写入EnhanceInfo，并返回true
     * 如果是常规的代理，仍然需要后续执行， 则返回false
     * @param enhanceInfo
     * @return
     */
    boolean preInvoke(EnhanceInfo enhanceInfo);

    /**
     * 在方法执行结束后调用
     * 将结果写入EnhanceInfo， 并且可以随意修改
     * @param enhanceInfo
     */
    void postInvoke(EnhanceInfo enhanceInfo);

    /**
     * 在方法返回后调用
     * 可以在这里做资源释放的操作
     * @param enhanceInfo
     */
    void postReturning(EnhanceInfo enhanceInfo);

    /**
     * 抛出异常时调用
     * 如果发生了异常，还想给外界返回一些信息，可以把结果写到EnhanceInfo中
     * @param enhanceInfo
     */
    void postThrowing(EnhanceInfo enhanceInfo);
}
