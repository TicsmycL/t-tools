package cn.ticsmyc.tools.aop;


import java.lang.reflect.Method;

/**
 * aop执行时需要的信息
 * @author Ticsmyc
 * @date 2020-12-21 21:45
 */

public class EnhanceInfo {

    //被代理的对象
    private Object proxy;
    //被执行的方法
    private Method method;
    //方法的参数列表
    private Object[] args;
    //抛出的异常
    private Exception exception;
    //返回值
    private Object result;


    public EnhanceInfo(Object proxy, Method method, Object[] args) {
        this.proxy = proxy;
        this.method = method;
        this.args = args;
    }

    public Object getProxy() {
        return proxy;
    }

    public void setProxy(Object proxy) {
        this.proxy = proxy;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
