package cn.ticsmyc.tools.packageScan.utils;

import java.net.URL;

/**
 * 用于转换包名的工具类
 * @author Ticsmyc
 * @date 2020-12-21 21:12
 */
public class PackageNameUtils {

    /**
     * 将包名的点好分隔符 替换成 /分割
     * @param name 包名转路径
     * @return 路径信息
     */
    public static String packageNameToPath(String name) {
        //第一个参数 正则表达式
        return name.replaceAll("\\.", "/");
    }

    /**
     * 截取类名，去掉类型
     * 如 "ticsmyc.class"  -> "ticsmyc"
     * @param classFileName
     * @return
     */
    public static String trimClassFileExtensionName(String classFileName){
        int pos = classFileName.indexOf('.');
        if(-1 != pos){
            return classFileName.substring(0,pos);
        }
        return classFileName;
    }

    /**
     * 根据URL获取路径， 去除前缀
     * 对于file协议，直接拿到路径 。 对于jar包内部，则拿到jar包的路径
     * 如 "file:/vip/ifmm/knapsack" - "/vip/ifmm/knapsack"
     *    "jar:file:/vip/ifmm/knapsack.jar!/vip/ifmm" - "/vip/ifmm/knapsack.jar"
     * @param url
     * @return
     */
    public static String getPathFromUrl(URL url){
        String fileUrl = url.getFile();

        //处理url指向jar包内部 的情况
        int i = fileUrl.indexOf('!');
        if(-1 ==i){
            return fileUrl;
        }
        return fileUrl.substring(5,i);
    }
}
