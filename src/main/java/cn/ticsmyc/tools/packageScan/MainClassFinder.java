package cn.ticsmyc.tools.packageScan;

/**
 * @author Ticsmyc
 * @date 2020-12-21 21:07
 */
public class MainClassFinder {
    /**
     * 通过抛出异常，检查调用栈 来获取启动类（main方法所在的类）
     * @return
     */
    public static String getMainClassName(){
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        return stackTrace[stackTrace.length-1].getClassName();
    }

}
