package cn.ticsmyc.tools.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.text.SimpleDateFormat;

/**
 * 配置好的json序列化器
 *
 * 依赖：
 *         <dependency>
 *             <groupId>com.fasterxml.jackson.core</groupId>
 *             <artifactId>jackson-databind</artifactId>
 *             <version>2.9.7</version>
 *         </dependency>
 *         <dependency>
 *             <groupId>com.fasterxml.jackson.core</groupId>
 *             <artifactId>jackson-core</artifactId>
 *             <version>2.9.7</version>
 *         </dependency>
 *         <dependency>
 *             <groupId>com.fasterxml.jackson.core</groupId>
 *             <artifactId>jackson-annotations</artifactId>
 *             <version>2.9.7</version>
 *         </dependency>
 * @author Ticsmyc
 * @date 2020-12-22 17:41
 */
public class JsonUtil {

    private static final ObjectMapper INSTANCE;
    static{
        INSTANCE = new ObjectMapper();

        //启动格式化输出
        INSTANCE.enable(SerializationFeature.INDENT_OUTPUT);

        //禁用后，没有setter方法和构造器的属性会被反序列化为null。（如果不禁用，直接抛异常）
        INSTANCE.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

        //遇到未知属性（没有setter 也没有这种属性的handler），不抛异常
        INSTANCE.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        /**
         * 通过该方法对mapper对象进行设置，哪些属性不序列化，所有序列化的对象都将按改规则进行系列化
         *  Include.Include.ALWAYS 默认
         *  Include.NON_DEFAULT 属性为默认值不序列化
         *  Include.NON_EMPTY 属性为 空（""） 或者为 NULL 都不序列化，则返回的json是没有这个字段的。这样对移动端会更省流量
         *  Include.NON_NULL 属性为NULL 不序列化
         */
        INSTANCE.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        //反序列化时， 空字符串"" 序列化为null
        INSTANCE.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);


        //允许使用 '\'' 来引用字符串 （正规json用的是 '\"'）
        INSTANCE.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        //允许在json串中使用 '#' 开头的注释
        INSTANCE.configure(JsonParser.Feature.ALLOW_YAML_COMMENTS, true);
        //允许在json串中使用 // 或者 /* 注释
        INSTANCE.configure(JsonParser.Feature.ALLOW_COMMENTS, true);

        //反序列化时，允许使用不带引号的字段名
        INSTANCE.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        //允许JSON字符串包含不带引号的控制字符（值小于32的ASCII字符，包括制表符和换行符）
        INSTANCE.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

        //将时间对象序列化为指定格式
        INSTANCE.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    }

    private JsonUtil(){}

    public static ObjectMapper getInstance(){
        return INSTANCE;
    }

}
