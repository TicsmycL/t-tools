package cn.ticsmyc.tools.framework.main;

import cn.ticsmyc.tools.framework.ioc.annotation.Autowired;
import cn.ticsmyc.tools.framework.ioc.annotation.Component;

/**
 * @author Ticsmyc
 * @date 2021-05-24 22:05
 */
@Component
public class WrapHello {

    @Autowired
    Hello hello;

    public void say(){
        System.out.println("wrap: ");
        hello.say();
        System.out.println("warp end");
    }
    public void say2(){
        System.out.println("wrap say :  "+ this +"  "+hello);
    }
}
