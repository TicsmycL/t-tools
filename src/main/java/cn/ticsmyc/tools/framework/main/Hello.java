package cn.ticsmyc.tools.framework.main;

import cn.ticsmyc.tools.framework.ioc.annotation.Autowired;
import cn.ticsmyc.tools.framework.ioc.annotation.Component;
import cn.ticsmyc.tools.framework.ioc.annotation.Value;

/**
 * @author Ticsmyc
 * @date 2021-05-24 17:39
 */
@Component
public class Hello {

    @Value("ticsmyc2")
    private String name;

    @Autowired
    private WrapHello wrapHello;

    public void say(){
        System.out.println("hello "+ name);
    }
    public void say2(){
        System.out.println("hello say :  "+this +"  " + wrapHello);
    }

    public Hello() {
    }

    public Hello(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
