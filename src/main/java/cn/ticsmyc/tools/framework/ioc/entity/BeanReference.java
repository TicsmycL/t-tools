package cn.ticsmyc.tools.framework.ioc.entity;

/**
 * 某个Bean的引用， 用于BeanDefinition的PropertyValue
 *
 * @author Ticsmyc
 * @date 2021-04-22 16:44
 */
public class BeanReference {

    private Object bean;
    private String beanId;

    public BeanReference(String beanId) {
        this.beanId = beanId;
    }




    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }
}
