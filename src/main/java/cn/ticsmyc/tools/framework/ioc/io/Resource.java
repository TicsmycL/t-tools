package cn.ticsmyc.tools.framework.ioc.io;

import java.io.InputStream;

/**
 * 资源
 * @author Ticsmyc
 * @date 2021-04-22 17:00
 */
public interface Resource {

    InputStream getInputStream()throws Exception;
}
