package cn.ticsmyc.tools.framework.ioc.context;

import cn.ticsmyc.tools.framework.ioc.factory.BeanFactory;

/**
 * @author Ticsmyc
 * @date 2021-04-22 16:07
 */
public abstract class AbstractApplicationContext implements ApplicationContext{

    BeanFactory beanFactory;
    @Override
    public Object getBean(Class clazz) throws Exception {
        return beanFactory.getBean(clazz);
    }

    @Override
    public Object getBean(String beanName) throws Exception {
        return beanFactory.getBean(beanName);
    }
}
