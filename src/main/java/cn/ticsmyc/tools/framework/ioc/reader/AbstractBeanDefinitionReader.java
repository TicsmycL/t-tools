package cn.ticsmyc.tools.framework.ioc.reader;

import cn.ticsmyc.tools.framework.ioc.entity.BeanDefinition;
import cn.ticsmyc.tools.framework.ioc.io.ResourceLoader;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ticsmyc
 * @date 2021-04-22 17:08
 */
public abstract class AbstractBeanDefinitionReader implements BeanDefinitionReader{

    /**
     * beanName -> BeanDefinition
     */
    private Map<String, BeanDefinition> registry;

    private ResourceLoader resourceLoader;

    public AbstractBeanDefinitionReader(ResourceLoader resourceLoader) {
        this.registry = new HashMap<>();
        this.resourceLoader = resourceLoader;
    }

    public Map<String, BeanDefinition> getRegistry() {
        return registry;
    }

    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
