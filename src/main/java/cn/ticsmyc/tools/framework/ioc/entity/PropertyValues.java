package cn.ticsmyc.tools.framework.ioc.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ticsmyc
 * @date 2021-04-22 16:13
 */
public class PropertyValues {

    private final List<PropertyValue> propertyValueList = new ArrayList<>();

    public PropertyValues() {
    }

    public void addPropertyValue(PropertyValue propertyValue){
        propertyValueList.add(propertyValue);
    }

    public List<PropertyValue> getPropertyValueList() {
        return propertyValueList;
    }
}
