package cn.ticsmyc.tools.framework.ioc.factory;


import cn.ticsmyc.tools.framework.ioc.entity.BeanDefinition;

/**
 * @author Ticsmyc
 * @date 2021-04-22 16:08
 */
public interface BeanFactory {
    Object getBean(Class clazz) throws Exception;
    Object getBean(String beanName) throws Exception;

    /**
     * 向工厂中注册BeanDefinition
     * @param beanName
     * @param beanDefinition
     * @throws Exception
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) throws Exception;

}
