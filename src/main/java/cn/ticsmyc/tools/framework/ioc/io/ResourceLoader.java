package cn.ticsmyc.tools.framework.ioc.io;

import java.net.URL;

/**
 * 通过路径加载本地文件 封装为UrlResource
 * @author Ticsmyc
 * @date 2021-04-22 17:00
 */
public class ResourceLoader {

    public Resource getResource(String location){
        URL resource = this.getClass().getClassLoader().getResource(location);
        return new UrlResource(resource);
    }
}
