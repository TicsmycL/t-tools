package cn.ticsmyc.tools.framework.ioc.entity;

/**
 * @author Ticsmyc
 * @date 2021-04-22 16:13
 */
public class PropertyValue {
    private final String name;
    /**
     * 如果Value是基本类型，则为值
     * 如果value是引用容器中其他bean，则类型为BeanReference
     */
    private final Object value;


    public PropertyValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
