package cn.ticsmyc.tools.framework.ioc.context;

/**
 * @author Ticsmyc
 * @date 2021-04-22 16:07
 */
public interface ApplicationContext {

    Object getBean(Class clazz) throws Exception;
    Object getBean(String beanName) throws Exception;

}
