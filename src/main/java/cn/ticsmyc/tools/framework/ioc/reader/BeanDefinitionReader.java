package cn.ticsmyc.tools.framework.ioc.reader;

/**
 * @author Ticsmyc
 * @date 2021-04-22 17:08
 */
public interface BeanDefinitionReader {

    /**
     * 从location加载BeanDefinition
     * @param location
     * @throws Exception
     */
    void loadBeanDefinition(String location)throws Exception;
}
