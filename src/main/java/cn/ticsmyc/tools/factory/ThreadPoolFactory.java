package cn.ticsmyc.tools.factory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池管理器， 管理一个单例的线程池
 *     /**************************************************************************************************************
 *      * 常见的几种线程技术
 *      **************************************************************************************************************
 *      * Java通过Executors提供四种线程池，分别为：
 *      * newCachedThreadPool创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
 *      * newFixedThreadPool 创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
 *      * newScheduledThreadPool 创建一个定长线程池，支持定时及周期性任务执行。 newSingleThreadExecutor
 *      * 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行。
 *      *
 *      * 1、public static ExecutorService newFixedThreadPool(int nThreads) {
 *      * return new ThreadPoolExecutor(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()); }
 *      *
 *      * 2、 public static ExecutorService newSingleThreadExecutor() {
 *      * return new FinalizableDelegatedExecutorService (new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>())); }
 *      *
 *      * 3、public static ExecutorService newCachedThreadPool() {
 *      * return new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>()); }
 *      ***************************************************************************************************************
 * @author Ticsmyc
 * @date 2020-08-14 15:34
 */
public class ThreadPoolFactory {

    // 线程池维护线程的最少数量
    private static final int SIZE_CORE_POOL = 15;
    // 线程池维护线程的最大数量
    private static final int SIZE_MAX_POOL = 15;
    // 生存时间
    private static final int KEEP_ALIVE_TIME = 1;
    // 阻塞队列容量
    private static final int BLOCKING_QUEUE_CAPACITY = 100;


    private volatile static ThreadPoolExecutor INSTANCE;
    /*
     * 线程池单例创建方法
     */
    public static ThreadPoolExecutor getInstance() {
        if(INSTANCE == null || INSTANCE.isShutdown()){
            synchronized (ThreadPoolFactory.class){
                if(INSTANCE == null || INSTANCE.isShutdown()){
                    INSTANCE = new ThreadPoolExecutor(SIZE_CORE_POOL, SIZE_MAX_POOL, KEEP_ALIVE_TIME,
                            TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(BLOCKING_QUEUE_CAPACITY));
                }
            }
        }
        return INSTANCE;
    }

    /*
     * 将构造方法访问修饰符设为私有，禁止任意实例化。
     */
    private ThreadPoolFactory() {
    }
}
