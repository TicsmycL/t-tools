package cn.ticsmyc.tools.multiThread.twoPhaseTermination;

/**
 * @author Ticsmyc
 * @date 2021-06-01 17:45
 */
public class DelegatingTerminatableThread extends AbstractTerminatableThread {
    private final Runnable task;

    public DelegatingTerminatableThread(Runnable task) {
        this.task = task;
    }
    
    public static AbstractTerminatableThread of(Runnable task) {
        DelegatingTerminatableThread ret = new DelegatingTerminatableThread(
                task);
        return ret;
    }

    @Override
    protected void doRun() {
        this.task.run();
    }


}
