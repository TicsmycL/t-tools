package cn.ticsmyc.tools.multiThread.twoPhaseTermination;

/**
 * @author Ticsmyc
 * @date 2021-06-01 16:39
 */
public interface Terminatable {

    void terminate();
}
