package cn.ticsmyc.tools.multiThread.producerConsumer.simple;

/**
 * @author Ticsmyc
 * @package fun.ticsmyc.Producerconsumer
 * @date 2020-03-06 11:58
 */
class PutThread implements Runnable {

    BlockQueue queue;

    public PutThread(BlockQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        int i = 0;
        for (; ; ) {
            i++;
            try {
                queue.put(i + "号");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}


