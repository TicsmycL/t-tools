package cn.ticsmyc.tools.multiThread.producerConsumer;

/**
 * 基于工作窃取算法的channel抽象
 *
 * @author Ticsmyc
 * @date 2021-06-02 16:57
 */
public interface WorkStealingEnabledChannel<P> extends Channel<P> {
    P take(int queueId) throws InterruptedException;
}
