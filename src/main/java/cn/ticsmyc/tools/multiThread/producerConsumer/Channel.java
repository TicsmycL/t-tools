package cn.ticsmyc.tools.multiThread.producerConsumer;

/**
 * 对普通通道的抽象
 *
 * @param <P> 产品类型
 * @author Ticsmyc
 * @date 2021-06-02 16:46
 */
public interface Channel<P> {

    /**
     * 从通道中取出一个产品
     *
     * @return
     * @throws InterruptedException
     */
    P take() throws InterruptedException;

    /**
     * 给通道中存入一个产品
     *
     * @param product
     * @throws InterruptedException
     */
    void put(P product) throws InterruptedException;

}
