package cn.ticsmyc.tools.multiThread.producerConsumer.simple;

/**
 * @author Ticsmyc
 * @package fun.ticsmyc.Producerconsumer
 * @date 2020-03-06 11:58
 */
class GetThread implements Runnable {

    BlockQueue queue;

    public GetThread(BlockQueue queue) {
        this.queue = queue;
    }


    @Override
    public void run() {
        for (; ; ) {
            //System.err.println("get： 1");

            for (int i = 0; i < queue.listSize; i++) {
                System.err.println("get： 开始了");
                try {
                    String value = queue.get(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}

