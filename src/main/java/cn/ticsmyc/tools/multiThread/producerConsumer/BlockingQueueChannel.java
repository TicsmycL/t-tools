package cn.ticsmyc.tools.multiThread.producerConsumer;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * 基于阻塞队列的channle实现
 *
 * @author Ticsmyc
 * @date 2021-06-02 16:49
 */
public class BlockingQueueChannel<P> implements Channel<P> {

    private BlockingQueue<P> blockQueue;

    protected final int DEFAULT_SIZE = 100;

    public BlockingQueueChannel() {
        this.blockQueue = new LinkedBlockingDeque<>(DEFAULT_SIZE);
    }

    public BlockingQueueChannel(BlockingQueue<P> blockQueue) {
        this.blockQueue = blockQueue;
    }

    @Override
    public P take() throws InterruptedException {
        return blockQueue.take();
    }

    @Override
    public void put(P product) throws InterruptedException {
        blockQueue.put(product);
    }
}
