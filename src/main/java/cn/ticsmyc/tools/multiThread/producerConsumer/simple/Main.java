package cn.ticsmyc.tools.multiThread.producerConsumer.simple;

/**
 * @author Ticsmyc
 * @package fun.ticsmyc.Producerconsumer
 * @date 2020-03-06 11:59
 */
public class Main {
    public static void main(String[] args) {
        BlockQueue queue = new BlockQueue(3);

        new Thread(new GetThread(queue)).start();
        new Thread(new PutThread(queue)).start();

    }
}
