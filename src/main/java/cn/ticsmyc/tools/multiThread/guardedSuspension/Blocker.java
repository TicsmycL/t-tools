package cn.ticsmyc.tools.multiThread.guardedSuspension;

import java.util.concurrent.Callable;

/**
 * @author Ticsmyc
 * @date 2021-05-31 15:51
 */
public interface Blocker {


    /**
     * 如果保护条件成立，就执行目标动作。 否则阻塞当前线程，知道保护条件成立。
     *
     * @param guardedAction 目标动作
     * @param <V>
     * @return
     * @throws Exception
     */
    <V> V callWithGuard(GuardedAction<V> guardedAction) throws Exception;

    /**
     * 执行stateOperation所指定的操作后，决定是否唤醒本Blocker所暂挂的所有线程中的一个线程。
     *
     * @param stateOperation 更改状态的操作，其call方法的返回值为true时，该方法才会唤醒被暂挂的线程
     */
    void signalAfter(Callable<Boolean> stateOperation) throws Exception;

    /**
     * 执行stateOperation所指定的操作后，决定是否唤醒本Blocker所暂挂的所有线程。
     *
     * @param stateOperation 更改状态的操作，其call方法的返回值为true时，该方法才会唤醒被暂挂的线程
     */
    void broadcastAfter(Callable<Boolean> stateOperation) throws Exception;

    void signal() throws InterruptedException;


}
