package cn.ticsmyc.tools.multiThread.guardedSuspension;

/**
 * @author Ticsmyc
 * @date 2021-05-31 15:49
 */
public interface Predicate {

    /**
     * 判断是否满足执行条件
     * @return
     */
    boolean evaluate();
}
