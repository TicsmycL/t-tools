package cn.ticsmyc.tools.multiThread.guardedSuspension;

import java.util.concurrent.Callable;

/**
 * @author Ticsmyc
 * @date 2021-05-31 15:50
 */
public abstract class GuardedAction<V> implements Callable<V> {

    protected final Predicate guard;

    public GuardedAction(Predicate guard){
        this.guard = guard;
    }

}
