package cn.ticsmyc.tools.multiThread.activeObject;

import java.util.concurrent.*;

/**
 * @author Ticsmyc
 * @date 2021-06-02 22:17
 */
public class Test {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyFun myFun = new MyFun();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Func func = ActiveObjectProxy.newInstance(Func.class, myFun, executorService);
        Future<String> cat = func.cat("123", "456");
        String s = func.doCat("avc", "def");
        System.out.println(s);
        System.out.println(cat.get());

        executorService.shutdown();
    }

}

interface Func {
    /**
     * 异步方法
     *
     * @param a
     * @param b
     */
    default Future<String> cat(String a, String b) {
        return null;
    }

    /**
     * 同步方法
     *
     * @param a
     * @param b
     */
    String doCat(String a, String b);
}

class MyFun implements Func {


    @Override
    public String doCat(String a, String b) {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return a + b;
    }
}
