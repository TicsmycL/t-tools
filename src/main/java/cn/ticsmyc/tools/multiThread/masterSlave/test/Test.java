package cn.ticsmyc.tools.multiThread.masterSlave.test;

import cn.ticsmyc.tools.multiThread.masterSlave.*;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @author Ticsmyc
 * @date 2021-06-04 16:16
 */
public class Test {

    public static void main(String[] args) throws SubTaskFailureException, ExecutionException, InterruptedException {

        Master master = new Master();
        master.fun();
    }
}

class Master extends AbstractMaster<Integer, Integer, Integer> {

    public Master() {
        init();
    }

    public void fun() throws SubTaskFailureException, ExecutionException, InterruptedException {
        //从1加到i  ,i从1到10

        Integer res = this.service(new int[]{1, 2, 3, 4, 5, 6, 7, 8});
        System.out.println(res);
    }


    @Override
    protected Integer combineResult(Iterator<Future<Integer>> iter) throws ExecutionException, InterruptedException {
        int res = 0;
        while (iter.hasNext()) {
            res += iter.next().get();
        }
        return res;
    }

    @Override
    protected TaskDivideStrategy<Integer> newTaskDivideStrategy(Object[] param) {
        return new Divide(param);
    }

    @Override
    protected Set<? extends SlaveSpec<Integer, Integer>> createSlaves() {
        Set<SlaveSpec<Integer, Integer>> set = new HashSet<>();
        for (int i = 0; i < 5; ++i) {
            set.add(new Worker());
        }
        return set;
    }
}

class Divide implements TaskDivideStrategy<Integer> {
    Deque<Integer> work;

    public Divide(Object[] param) {
        this.work = new LinkedList<>();
        int[] num = (int[]) param[0];
        for (int i = 0; i < num.length; i++) {
            work.add(num[i]);
        }
        System.out.println("任务分割： 共有" + work.size() + "个任务");
    }

    @Override
    public Integer nextChunk() {
        if(work.isEmpty()) {
            return null;
        }
        return work.poll();
    }
}

class Worker extends SlaveWorkerThread<Integer, Integer> {

    public Worker() {
        super(new LinkedBlockingDeque<>());
    }

    @Override
    protected Integer doProcess(Integer task) {
        System.out.println(Thread.currentThread().getName() + "拿到任务 " + task);
        int res = 0;
        for (int i = 0; i <= task; ++i) {
            res += i;
        }
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }
}
