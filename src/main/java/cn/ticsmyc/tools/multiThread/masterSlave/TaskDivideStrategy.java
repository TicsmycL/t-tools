package cn.ticsmyc.tools.multiThread.masterSlave;

/**
 * 任务分解策略抽象
 *
 * @author Ticsmyc
 * @date 2021-06-03 17:44
 */
public interface TaskDivideStrategy<T> {

    /**
     * 下一个任务
     * 当没有下一个任务时，返回null
     */
    T nextChunk();
}
