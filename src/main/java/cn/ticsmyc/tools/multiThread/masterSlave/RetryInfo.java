package cn.ticsmyc.tools.multiThread.masterSlave;

import java.util.concurrent.Callable;

/**
 * 子任务重试所需的信息
 *
 * @author Ticsmyc
 * @date 2021-06-04 11:08
 */
public class RetryInfo<T, V> {
    public final V subSTask;
    public final Callable<T> redoCommand;

    public RetryInfo(V subSTask, Callable<T> redoCommand) {
        this.subSTask = subSTask;
        this.redoCommand = redoCommand;
    }
}
