package cn.ticsmyc.tools.multiThread.masterSlave;

import java.util.concurrent.Future;

/**
 * 任务执行slave的抽象
 *
 * @param <T> 返回类型
 * @param <V> 任务类型
 * @author Ticsmyc
 * @date 2021-06-03 17:45
 */
public interface SlaveSpec<T, V> {

    /**
     * 启动这个slave线程
     */
    void init();

    /**
     * 终止slave线程
     */
    void shutdown();

    /**
     * 提交任务到slave执行
     *
     * @param task
     * @return
     * @throws InterruptedException
     */
    Future<T> submit(final V task) throws InterruptedException, SubTaskFailureException;

}
