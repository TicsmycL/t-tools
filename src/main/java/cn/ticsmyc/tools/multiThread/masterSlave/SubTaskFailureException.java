package cn.ticsmyc.tools.multiThread.masterSlave;

/**
 * 子任务处理失败后的异常
 *
 * @author Ticsmyc
 * @date 2021-06-04 11:06
 */
public class SubTaskFailureException extends Exception {

    /**
     * 子任务重试所需的信息
     */
    private final RetryInfo retryInfo;

    public SubTaskFailureException(RetryInfo retryInfo, Exception cause) {
        super(cause);
        this.retryInfo = retryInfo;
    }

    public RetryInfo getRetryInfo() {
        return retryInfo;
    }
}
