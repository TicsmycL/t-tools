package cn.ticsmyc.tools.multiThread.masterSlave;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Future;

/**
 * 子任务分发策略抽象
 *
 * @param <T> 返回类型
 * @param <V> 参数类型
 * @author Ticsmyc
 * @date 2021-06-03 17:48
 */
public interface SubTaskDispatchStrategy<T, V> {

    /**
     * @param slaves             slave列表
     * @param taskDivideStrategy 任务分解策略
     * @return 子任务的Future集合的迭代器
     * @throws InterruptedException 被中断时抛异常
     */
    Iterator<Future<T>> dispatch(Set<? extends SlaveSpec<T, V>> slaves, TaskDivideStrategy<V> taskDivideStrategy)
            throws InterruptedException, SubTaskFailureException;
}
