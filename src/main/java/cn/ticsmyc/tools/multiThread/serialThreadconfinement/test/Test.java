package cn.ticsmyc.tools.multiThread.serialThreadconfinement.test;

import cn.ticsmyc.tools.multiThread.serialThreadconfinement.AbstractSerializer;
import cn.ticsmyc.tools.multiThread.serialThreadconfinement.TaskProcessor;

import java.util.concurrent.*;

/**
 * @author Ticsmyc
 * @date 2021-06-03 16:20
 */
public class Test {
    static int count = 1;

    public static void main(String[] args) {
        MethodSerializer mySerializer = new MethodSerializer(new LinkedBlockingDeque<>(), new MyStringProcessor());
        mySerializer.init();
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; ++i) {
            executorService.submit(() -> {
                try {
                    String res = mySerializer.function("asdflkj");
//                    String res = mySerializer.functionSync("asdflkj");
                    System.out.println(res);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
    }
}

class MethodSerializer extends AbstractSerializer<String, String> {


    public MethodSerializer(BlockingQueue<Runnable> blockingQueue, TaskProcessor<String, String> taskProcessor) {
        super(blockingQueue, taskProcessor);
    }

    public String functionSync(String word) throws ExecutionException, InterruptedException {
        Future<String> service = service(word);
        return service.get();
    }

    public String function(String word) throws Exception {
        MyStringProcessor myStringProcessor = new MyStringProcessor();
        return myStringProcessor.doProcess(word);
    }

    /**
     * 用于转换普通方法的参数到串行方法的参数
     *
     * @param params
     * @return
     */
    @Override
    protected String getTaskParam(Object[] params) {
        return (String) params[0];
    }

}

class MyStringProcessor implements TaskProcessor<String, String> {
    /**
     * 对指定任务进行处理
     *
     * @param word
     * @return
     * @throws Exception
     */
    @Override
    public String doProcess(String word) throws Exception {
        return toUpperCase(word);
    }

    public String toUpperCase(String word) throws InterruptedException {
        System.out.println(Test.count++);
        TimeUnit.SECONDS.sleep(1);
        return word.toUpperCase();
    }
}