package cn.ticsmyc.tools.multiThread.serialThreadconfinement;

/**
 * 任务处理的抽象
 * 将需要并行转串行执行的方法写在doProcess中， 参数为方法所需参数
 *
 * @param <T> 返回类型
 * @param <V> 任务类型
 * @author Ticsmyc
 * @date 2021-06-03 16:04
 */
public interface TaskProcessor<T, V> {


    /**
     * 对指定任务进行处理
     *
     * @param task
     * @return
     * @throws Exception
     */
    T doProcess(V task) throws Exception;
}
